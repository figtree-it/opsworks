default[:taiga][:packages] 	= [
							'build-essential',
							'binutils-doc',
							'flex',
							'bison',
							'libjpeg-dev',
							'libfreetype6-dev',
							'zlib1g-dev',
							'libzmq3-dev',
							'libgdbm-dev',
							'libncurses5-dev',
							'automake',
							'libtool',
							'libffi-dev',
							'curl',
							'tmux',
							'gettext',
							'python3',
							'python3-pip',
							'python-dev',
							'python3-dev',
							'python-pip',
							'virtualenvwrapper',
							'libxml2-dev',
							'libxslt-dev'
							]
default[:taiga][:pip_modules] = [
							{ pkg: 'gunicorn' 	, version: 'latest'   },
							{ pkg:'django'      , version:'1.8.5' },
							{ pkg: 'circus' 	, version: 'latest'   }
							]							

default[:taiga][:opsworks_app_names][:frontend] = "taiga_backend"
default[:taiga][:opsworks_app_names][:backend] 	= "taiga_backend"
default[:taiga][:opsworks_app_names][:events] 	= "taiga_backend"
default[:taiga][:settings][:db_name]			= "taiga"
