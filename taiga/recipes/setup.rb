node[:taiga][:packages].each do |pkg|
  package pkg do
    action :install
  end
end


node[:taiga][:pip_modules].each do |mod_def| 
  if mod_def[:version] == 'latest'
    python_pip mod_def[:pkg]    
  else
    python_pip mod_def[:pkg] do 
      version mod_def[:version]
    end
  end
end