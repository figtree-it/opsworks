frontend_app_name = node[:taiga][:opsworks_app_names][:frontend]
backend_app_name = node[:taiga][:opsworks_app_names][:backend]
events_app_name = node[:taiga][:opsworks_app_names][:events]
node[:deploy].each do |application, deploy|
	if 	deploy[:application] == frontend_app_name or
		deploy[:application] == backend_app_name  or
		deploy[:application] == events_app_name 

	  	opsworks_deploy_dir do
	  	  user deploy[:user]
	  	  group deploy[:group]
	  	  path deploy[:deploy_to]
	  	end

	  	opsworks_deploy do
	    	app application
	    	deploy_data deploy
	  	end
  	end   	
  	if deploy[:application] == backend_app_name 
		env        = node[:deploy][backend_app_name][:environment]['environment']
		settings   = node[:taiga][:settings][env]
		
		ruby_block 'Create new virtualenv for Taiga' do
		  block do
		    %x[ sudo source /usr/share/virtualenvwrapper/virtualenvwrapper.sh ]
		    %x[ sudo mkvirtualenv -p /usr/bin/python3.4 taiga  ]
		  end
		end 
		
		ruby_block 'Install Requirements' do
		  block do
		    %x[ pip install -f  #{deploy[:deploy_to]}/current/requirements.txt  ]
		  end
		end 

		db = node[:opsworks][:stack][:rds_instances][0]
		
		template "#{deploy[:deploy_to]}/current/settings/local.py" do
		  source "local.py.erb"
		  mode 0644
		  owner "www-data"		  		 
		  variables(
		    'db' => {
		      user: db[:db_user],
		      pass: db[:db_password],
		      name: node[:taiga][:settings][:db_name],
		      host: db[:address],
		      port: 5432
		    } ,
		    'env'=> env

		  )
		end		
  	end
end